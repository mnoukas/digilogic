------------------------------------------------------------------------------
--  File: mux.vhd
------------------------------------------------------------------------------
--Multiplexer design
--Control signal is CONT_SIG
--The output is chosen from 2 signals with length 8 bits
--If control signal is '1' then input A_in is chosen 
--If '0' then input B_IN is chosen to the output OUT_SIG


  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         CONT_SIG : in  std_logic;
         OUT_SIG_BEH  : out std_logic;
         OUT_SIG_FLOW  : out std_logic;
         OUT_SIG_STRUCT  : out std_logic);
  end Mux;
  
architecture RTL of Mux is
  -- AND GATE component
  component AND_GATE is
  port (	A_IN    : in std_logic;
  			B_ID	: in std_logic;
  			AND_OUT	:	out std_logic;
  		);
  end component;
  
  -- OR GATE component
  component OR_GATE is
  port (	A_IN    : in std_logic;
  			B_ID	: in std_logic;
  			OR_OUT	:	out std_logic;
  		);
  end component;
  
  -- inverter component
  component INVERTER is
  port (	INVERTER_IN    : in std_logic;
  			INVERTER_OUT	:	out std_logic;
  		);
  end component;
  
  signal A_OUT, B_OUT, NOT_CONT : std_logic;
begin
 
--Behavioural process DISP_MUX
DISP_MUX: process ( A_IN, B_IN, CONT_SIG ) --sensitivity list
  begin
    if CONT_SIG = '1' then
       	OUT_SIG_BEH <= A_IN;
    elsif CONT_SIG = '0' then
    	OUT_SIG_BEH <= B_IN;
    else
       	OUT_SIG_BEH <= 'X';
    end if;
end process DISP_MUX;

OUT_SIG_FLOW  <= (A_IN and CONT_SIG) or (B_IN and not (CONT_SIG));

G1: AND_GATE port map(A_IN, CONT_SIG, A_OUT);
G2: AND_GATE port map(B_IN, NOT_CONT, B_OUT);
G3: INVERTER port map(INVERTER_IN => CONT_SIG, INVERTER_OUT => NOT_CONT);
G4: OR_GATE port map(A_OUT, B_OUT, OUT_SIG_STRUCT);

end RTL;