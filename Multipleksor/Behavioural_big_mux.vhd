library IEEE;
use IEEE.std_logic_1164.all;
 
-- käitumuslik olem koos sisendite, signaalide ja väljundiga
entity Behavioural_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end Behavioural_big_mux;

-- käitumusliku mudeli tingimuslaused ning väljundi out_3 väärtustamine vastavalt signaalidele a, b
architecture RTL of Behavioural_big_mux is
begin
  process ( D0, D1, D2, D3, a, b ) begin
	if  a = '1' then
		if b = '1' then
			out_3 <= D3;
		else
			out_3 <= D1;
		end if;
	else
		if b = '1' then
			out_3 <= D2;
		else
			out_3 <= D0;
		end if;
	end if;
  end process;
end RTL;