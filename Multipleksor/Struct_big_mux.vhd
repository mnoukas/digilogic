library IEEE;
use IEEE.std_logic_1164.all;
 
 -- struktuurne olem "suuremast komponendist"
entity Struct_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(0 to 4);
		a, b : in std_logic;
		out_3 : out std_logic_vector(0 to 4)
	);
end Struct_big_mux;
 
architecture RTL of Struct_big_mux is
 
 -- anname komponendiks valmistehtud struktuurse mudeliga �he muxi
component Struct_mux is
	port(
		f, s : in std_logic_vector(0 to 4);
		c : in std_logic;
		y : out std_logic_vector(0 to 4)
	);
end component Struct_mux;
 
 -- abisignaalid
signal out_1, out_2 : std_logic_vector(0 to 4);
 
 -- tekitame kolm muxi
 -- alamkomponentide initsialiseerimine
begin
	sm_1 : Struct_mux
		port map(
			f => D1,
			s => D0,
			c => a,
			y => out_1
		);
	sm_2 : Struct_mux
		port map(
			f => D3,
			s => D2,
			c => a,
			y => out_2
		);
	sm_3 : Struct_mux
		port map(
			f => out_2,
			s => out_1,
			c => b,
			y => out_3
		);
 
end RTL;