library ieee;
use ieee.std_logic_1164.all;

--Not-and v�rava olemi tegemine koos sisendite ja �he v�ljundiga
entity NotAndGate is
   port( 
	x0, x1 : in std_logic_vector(4 downto 0);
        out_nand : out std_logic_vector(4 downto 0)
	);
end NotAndGate;

-- not-and v�rava loogika
architecture RTL of NotAndGate is 
begin
   out_nand <= not(x0 and x1);
end RTL;