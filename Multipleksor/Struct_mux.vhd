library IEEE;
use IEEE.std_logic_1164.all;

-- struktuurne olem "alamkomponendist"
entity Struct_mux is
	port(
		f, s : in std_logic_vector(4 downto 0);
		c : in std_logic;
		y : out std_logic_vector(4 downto 0)
	);
end Struct_mux;

-- kasutame valmistehtud Not-and väravat
architecture RTL of Struct_mux is
component NotAndGate
  port ( 
	x0, x1 : in std_logic_vector(4 downto 0);
        out_nand : out std_logic_vector(4 downto 0)
	);
end component;
 
 --abisignaalid
signal f_c, s_cc: std_logic_vector(4 downto 0);
signal c_c: std_logic_vector(4 downto 0);
signal c_5 : std_logic_vector(4 downto 0);

-- tingimuslaused ning signaalide väärtustamine
begin
	process(c) begin 
		if c ='1' then
			c_5 <= "11111";
		else 
			c_5 <= "00000";
		end if;
	end process;

-- alamkomponentide initsialiseerimine
	U1: NotAndGate 
	port map(
		x0=>c_5, 
		x1=>c_5, 
		out_nand=>c_c
		);

	U2: NotAndGate 
	port map(
		x0=>f, 
		x1=>c_5, 
		out_nand=>f_c
		);

	U3: NotAndGate 
	port map(
		x0=>c_c, 
		x1=>s,
		out_nand=>s_cc
		);

	U4: NotAndGate 
	port map(
		x0=>f_c,
		x1=>s_cc,
		out_nand=>y
		);
end RTL;