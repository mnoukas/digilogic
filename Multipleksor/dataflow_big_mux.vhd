library IEEE;
use IEEE.std_logic_1164.all;

-- andmevoo olem
entity dataflow_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end dataflow_big_mux;


architecture RTL of dataflow_big_mux is
signal a_5, b_5 : std_logic_vector(4 downto 0);

-- tingimused ning vastavalt abisignaalidele v��rtuste andmised
begin
process(a,b) begin 

	if a ='1' then
		a_5 <= "11111";
	else 
		a_5 <= "00000";
	end if;
	
	if b ='1' then
		b_5 <= "11111";
	else 
		b_5 <= "00000";
	end if;

end process;

-- andmevoo lause, mis annab meie v�ljundile out_3 v��rtuse
	out_3 <= (((not a_5) and (not b_5) and D0)or((not a_5) and b_5 and D2)or(a_5 and (not b_5) and D1)or(a_5 and b_5 and D3));

end RTL;