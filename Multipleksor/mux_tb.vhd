------------------------------------------------------------------------------
--  File: mux_tb.vhd
------------------------------------------------------------------------------
--Multiplexor testbench
 library IEEE;
use IEEE.std_logic_1164.all;

entity mux_tb is
end mux_tb;

architecture testbench of mux_tb is

-- siin testime k�ike kolme mudelit
-- deklareerime k�igepealt komponendid (olemid) eraldi

--struktuurne
component Struct_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end component Struct_big_mux;

-- k�itumuslik
component Behavioural_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end component Behavioural_big_mux;

-- andmevoog
component Dataflow_big_mux is
	port(
		D0, D1, D2, D3 : in std_logic_vector(4 downto 0);
		a, b : in std_logic;
		out_3 : out std_logic_vector(4 downto 0)
	);
end component Dataflow_big_mux;

-- abisignaalid
signal D0_tb, D1_tb, D2_tb, D3_tb, Struct_out, Beh_out, Data_out : std_logic_vector (4 downto 0);
signal a_tb, b_tb : std_logic;

-- j�llegi alamkomponentide initsialiseerimine
begin
	stru_m : Struct_big_mux
		port map(
			D0 => D0_tb,
			D1 => D1_tb,
			D2 => D2_tb,
			D3 => D3_tb,
			a => a_tb,
			b => b_tb,
			out_3 => Struct_out
		);

	beh_m : Behavioural_big_mux
		port map(
			D0 => D0_tb,
			D1 => D1_tb,
			D2 => D2_tb,
			D3 => D3_tb,
			a => a_tb,
			b => b_tb,
			out_3 => Beh_out
		);

	data_m : Dataflow_big_mux
		port map(
			D0 => D0_tb,
			D1 => D1_tb,
			D2 => D2_tb,
			D3 => D3_tb,
			a => a_tb,
			b => b_tb,
			out_3 => Data_out
		);

-- testbench abisignaalide omistamine 
	process
	begin
		D0_tb <= "00000";
		D1_tb <= "11111";
		D2_tb <= "11110";
		D3_tb <= "00000";
		
		a_tb<='0';
		b_tb<='0';
		wait for 2 ms;
 
		a_tb<='0';
		b_tb<='1';
		wait for 2 ms;
 
		a_tb<='1';
		b_tb<='0';
		wait for 2 ms;
 
		a_tb<='1';
		b_tb<='1';
		wait for 2 ms;

		wait;
	end process;	
end testbench;